# DOs and DON'Ts when building a Yocto based distribution

## Track
Embedded, Mobile and Automotive

https://fosdem.org/2022/schedule/track/embedded_mobile_and_automotive/

## Title
DOs and DON'Ts when building a Yocto based distribution

## Subtitle
What we have learned in a year of building Oniro

## Event type
20 min talk + 5 min QA

## Persons
Bernhard "bero" Rosenkränzer

Bero has been a Linux developer for 25 years - working on
desktop, server and embedded alike.
He is a regular speaker at Open Source conferences like
FOSDEM, Embedded Linux Conference, Linux Piter and Open
Source Summit.
He currently works on the Eclipse Foundation's Oniro
operating system. Previous employers include Linaro
and Red Hat.

## Abstract (one paragraph)
After approximately a year of work, Oniro - the Eclipse
Foundation's embedded operating system - has been released
to the public, and is open to bigger changes like rebasing
to a newer Yocto branch and updating toolchains. Now we're
seeing - and can share - what we did well and what we will
have to do differently in the future.

## Description
After approximately a year of work,
Oniro (https://oniroproject.org/) - the Eclipse
Foundation's embedded operating system - has been released
to the public, and is open to bigger changes like rebasing
to a newer Yocto branch and updating toolchains. Now we're
seeing - and can share - what we did well and what we will
have to do differently in the future.

We updated a number of packages from dunfell - can this be
avoided or done in a better way?
What can we do about packages that don't have an LTS policy?

An important goal must be to make rebasing to newer upstream
releases as quick and painless as possible - accumulating
as little technical debt as we can.
