# GPIO across Linux and Zephyr: Maximizing code reuse in the Oniro doorlock blueprint

## Track
Embedded, Mobile and Automotive
https://fosdem.org/2022/schedule/track/embedded_mobile_and_automotive/

## Title
GPIO across Linux and Zephyr kernels

## Subtitle
Maximizing code reuse in the example of the Oniro doorlock blueprint

## Event type
20 min talk + 5 min QA

## Persons
Bernhard "bero" Rosenkränzer

Bero has been a Linux developer for 25 years - working on
desktop, server and embedded alike.
He is a regular speaker at Open Source conferences like
FOSDEM, Embedded Linux Conference, Linux Piter and Open
Source Summit.
He currently works on the Eclipse Foundation's Oniro
operating system. Previous employers include Linaro
and Red Hat.

## Abstract (one paragraph)
Sometimes it is useful to share code across multiple kernels -- in projects
like the Oniro door lock blueprint, for the typical use cases a Cortex-M
CPU running Zephyr is more than sufficient, but using its functionality as
part of a larger project makes using Linux on Cortex-A an interesting
option. Can we find a way to maximize code reuse despite the very different
GPIO APIs?

## Description
Sometimes it is useful to share code across multiple kernels -- we avoid
doing the work twice. However, this might be complicated when one version
is using Zephyr with a Cortex-M, and another Linux on a Cortex-A.
We will explore this problem using an example of the door lock blueprint
from the Eclipse Oniro project - an implementation of basic door lock
functionality.
For a typical use-case, a Cortex-M with Zephyr is more than sufficient,
but we might also want to integrate the same application in a bigger one
using Linux on a Cortex-A. Will we find a way to do so?
