# Oniro WG Incubation stage marketing 2022wk02

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-01-12

## Participants

(add/erase participants)

Shanda (EF), Agustin B.B.(EF), Gael (EF), Adrian (Huawei), Patrick O. (NOI), Donghi (Huawei), Andrea G. (Linaro), Davide R. (Huawei), Andrea B. (Synesthesia), Aurore P. (Huawei), Philippe (Huawei), Chiara (Huawei), Carlo (Array),  


## Agenda

* Events list - Chiara 10 min
* FOSDEM - Philippe Coval 10 min
* AOB - 10 min

## MoM

### Events list

Presentation

* #link to the events list: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/Events-oniro 
* Chiara shows the events list (check the link above). 
* Wiki page per event to manage it and present the key information plus the assets on git.



Discussion

* NOI Techpark has not appointed a marketing person so there are no events coming from NOI in the event list. Partick ack.



### FOSDEM

Presentation

* FOSDEM wiki page at Oniro WG wiki #link  https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/FOSDEM
* Oniro stand at FOSDEM #link https://stands.fosdem.org/stands/oniro_project/ 
* Eclipse Foundation Stand at FOSDEM https://stands.fosdem.org/stands/eclipse-foundation/
* Social media strategy and plan
* Press release.

Discussion

* #task Add on the landing page a link/button to the mailing list.
* #task Add to the TLP the communication channels since we will be adding the link to this page at the Oniro FOSDEM talks.
* Press release content: maybe talk about Goofy.


### AOB


Points coming from the Marketing task force meeting 2022-01-11

* How do we feed social media messages so EF publish them?
* Add the Tuesday Marketing task Force meetings in the Oniro calendar and announce it on the mailing list.

Other points

* Carlo proposes to improve the landing page. It is not at the level it should be.
* Davide talks about gather.town Donghi is playing with it to see if it works for us. Donghi shows it. 
   * Andrea: Linaro tried this last year. He suggests 3D. Kristine from Linaro is the point of contact.
   * Philippe talks about the Mozilla option. He shows a video of a hack he did. 

## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
