# Oniro WG Incubation stage marketing 2022wk07

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-02-16

## Participants

Carlo P. (Array), Chiara DF (Huawei), Philippe C. (Huawei), Dony (Huawei), Davide R. (Huawei), Adrian O'S(Huawei), Gael B (EF), Clark R. (EF), Agustin BB (EF),  

## Agenda

* MWC - Chiara 15 min
* New events - 5 min
* IP transfer - 5 min
* News - 3 min
* AOB - 2 min

No agenda sent up front sso we started by agreeing on the agenda. Two additional points added.

## MoM

### MWC 2022

Meetings

* Confirmed breakfast with Canonical on March 1st
   * We need to think about an elevator peach for Canonical
* More invites sent out.

Presentation

* March 1st at 9:00 there will be a f2f/online pressentation about Oniro under invitation.
   * All the information is in the MWC wiki page.
   * The presentation is not at MWC venue but at W Barcelona hotel. OSC, another EF project, will have a presentation that day too.
   * Video will go to the Oniro playlits
   * Member reps. will participate. Can we add members to the equation?
      * davide will be there
      * Marco from Seco is attending to the presentation
      * Will Linaro join?
* Media resources confirmed.
   
Collateral

* We would like to have a collateral to hand over at the event and to the participants.
* There was a one page done for FODEM. #link <https://drive.google.com/file/d/1XZj_JCRVa3qeuNLtOTM0KVsOFsK0Y7eu/view> This will be the source to work with.
   * This is an infographic not a hand out.
   * We need a new version for the handout. Chiara might make a proposal.

Links
* The ticket to track the invitation creation and distribution is #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/29>
* Wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/MWC>

Discussion


### New events

Presentation

* Huawei community event (open webminars)
   * #link: <https://www.meetup.com/hdg-italia/> <https://www.meetup.com/hdg-germany/> <https://www.meetup.com/hdg-uk/> <https://www.meetup.com/hdg-spain/> <https://www.meetup.com/hdg-poland/> 
      * Plan to have presence on regular basis
      * Management of these events is not centralised. A little extra effort for Chiara
      * French one #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/11>
* FOSSNorth #link <https://foss-north.se/2022/>
   * 7 Oniro talks submitted.
   * Huawei is sponsoring.
   * Wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/Foss-North>
* IoTS WC
   * Go over it in the next occassion.
   * #link <https://www.iotsworldcongress.com/>
* Agustin will propose to have our community gathering during EclipseCon 2022, around the last few days of October. Agustin will send a more detailed proposal in this regard.
   
Discussion


### News

Marketing Committee

* It will be scheduled for Wednesday March 9th at this same time during 1 hour
   * Check the Oniro WG Charter for information about goals and responsibilities for this Committee #link <https://www.eclipse.org/org/workinggroups/oniro-charter.php>

Marketing Plan

* Next Wednesday in this slot, description about the Marketing Plan
   * EF process for developing the Marketing Plan and example

Other news:

* Marketing disscussion mailing list is coming. Please make sure you have a EF account:
   * #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Join_Oniro>

   
Discussion

### IP transfer of designs and marketing material from HUawei to EF

* Chiara ask how this is going.
* Agustin mentions that EF will discuss this topic 1:1 with HUawei representatives since it involves EF and Huawei (although affects Oniro). This could be a topic even in the 1: Huawei:ef/Oniro or the coming visit to Milan (Gael B). 

### AOB



## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
