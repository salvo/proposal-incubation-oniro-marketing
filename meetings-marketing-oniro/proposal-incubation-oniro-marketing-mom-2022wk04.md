# Oniro WG Incubation stage marketing 2022wk04

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-01-26

## Participants

Yves M.(EF), Clark R. (EF), Agustin B.B.(EF), Donghi (Huawei), Chiara (Huawei), Carlo (Array), Adrian O'S (Huawei), Aurore (Huawei).  

## Agenda

* FOSDEM - Chiara/Philippe/Agustin 15 min
* Social media management - Shanda 5 min
* Content IP - Agustin 5 min
* AOB - 5 min

## MoM

### FOSDEM

Presentation

* Website
   * What changes do we want to add to the landing page for FOSDEM?
   * We have a ticket to describe and track those #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/18>
* Stands
   * Oniro content has been proposed to the EF stand. Thanks Philippe.
* News
   * #task #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/28>
* Talks
   * Videos are uploaded to FOSDEM infra.
* Social media campaign
   * Donghi provided an initial set of messages for the campaign. Thanks Donghi.
   * Live tweeting. Chiara is working on guidelines focussed on reactions.
* Other

Discussion


* Website
   * #task Agustin can we involve somebody from the EF marketing/webdev team in a meeting (next Tuesday) to discuss changes in the design? 
   * #task We all want a blog. Create a work stream to define the action of creating a blog.
* Stands
   * Pending point is the infographics
   * # task next week we will go over the management of the stand during the event.
* News
   * We might want to add a news entry about the migration of tools by MWC 
* Talks
* Social media campaign
   * Agustin: can we add to that campaign or agree on a second campaign to be run between FOSDEM and MWC where we promote every video from each talk uploaded to our Youtube channel? We will have 14 videos and around 20 days. One per day?
   * #task Agustin to find out how we can promote Oniro talks among the IoT & Edge communities at Eclipse.
* Other


### Social media management

Presentation

* Tasks from the previous meeting:
   * #task Social media administrator. Who should that person be?
      * As defined in the ticket <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/23> we will add Chiara. Is there any opposition to have Donghi as alternate?
      * These positions will be confirmed or not by the Oniro WG. 
   * #task Plugin and tool to manage the account inside Oniro by the social admin. Which tool should that be.
      * Until we find the right plug-in to a tool, we can use as Oniro, we will go for the simplest solution, that works well for other WG since the amount of messages out of a campaign are low, we will use Tweetdeck. Any opposition to this measure?

Discussion

* Agustin will send his proposal to the mailing list for discussion

### Content IP

Presentation

Problem statement:
* We value the work provided by the agency that Huawei is working with. We want to use that work without risks, not just within Oniro but also in the context of each Member organization.
* EF is the copyright holder of Oniro. This is one of the core values of a vendor neutral environment. All Members can use the brand safely following the guidelines.
* EF has no relation with such agency so they cannot use the brand without consent from EF. Even if they do, the outcome has to be owned by EF in order for any company to use it without risks.
* Given that the agency is working on material on continuous basis, the one time solution we applied to the logo and the rest of the initial contribution is not practical.

So we need to create a two way relation between EF and the agency. This relation can be a three parties relation.

Gael and Agustin, have had an initial conversation with Huawei to describe the problem statement. We reached a common understanding of the challenge. We will work on defining a solution in the coming days and then execute it. 

So for now we will not use additional output coming from the agency until we sort out a sustainable solution. Sorry for the inconveniences.


Discussion

* Can we add an annex to the existing agreement including the material for FOSDEM?
   * #task Agustin/Clark will work internally at EF to push this or similar solution so we can use the existing material for FOSDEM 

### AOB

* Link between mastodon and twitter. Deadline next Monday at 15:00 CET
* We need contacts for MWC meetings. #task Agustin send a reminder before Friday's meeting about this.


## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
